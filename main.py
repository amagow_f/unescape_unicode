import sys
import fire 

def unescape(input_file, output_file):
    unescape_helper = lambda x: x.encode('utf-8').decode('unicode-escape')
    with open(input_file, "r") as file:
        input_data = file.readlines()
        escaped = list(map(unescape_helper, input_data))
        output_data = "".join(escaped)

    with open(output_file, "w") as file:
        file.write(output_data)

def main():
    fire.Fire(unescape)
    
if __name__ == "__main__":
    sys.exit(main())
